import { Component, OnInit } from '@angular/core';
import SockJS from "sockjs-client"
import { Stomp } from "@stomp/stompjs"

@Component({
  selector: 'app-main-customer',
  templateUrl: './main-customer.component.html',
  styleUrls: ['./main-customer.component.css']
})
export class MainCustomerComponent implements OnInit {
  private serverUrl = '/api/customers'
  private stompClient;
  private message = '';

  constructor() { 
    this.initializeWebSocketConnection();
  }

  ngOnInit() {
  }

  initializeWebSocketConnection(){
    let ws = new SockJS(this.serverUrl);

    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe("/customerRequest", (message) => {
        if(message.body) {
          console.log("HOME RECEBENDO A MSG");
          console.log(message.body);
        }
      });
    });
  }

  sendMessage(message){
    console.log("REQUEST UBER "+message);
    this.stompClient.send("/app/send/message" , {}, message);
  }


}
