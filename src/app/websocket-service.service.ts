import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class WebsocketServiceService {

  private socket;

  constructor() { }

  connect(): void {
    this.socket = io.connect("http://localhost:8080/api/socket");
        this.socket.on('event', (data) => {
      console.log(JSON.stringify(data));
    });
  }

}
