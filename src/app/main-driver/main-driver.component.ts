import { Component, OnInit } from '@angular/core';
import SockJS from "sockjs-client"
import { Stomp } from "@stomp/stompjs"
import { DriverServiceService } from 'src/app/driver-service.service'

@Component({
  selector: 'app-main-driver',
  templateUrl: './main-driver.component.html',
  styleUrls: ['./main-driver.component.css']
})
export class MainDriverComponent implements OnInit {
  private serverUrl = '/api/drivers'
  private stompClient;
  private service:DriverServiceService;
  constructor(private driverServiceProxy:DriverServiceService) {
    this.service = driverServiceProxy;
   }

  ngOnInit() {
    this.initializeWebSocketConnection(this.service);
  }

  initializeWebSocketConnection(service){
    
    let ws = new SockJS(this.serverUrl);

    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe("/driverStreamingRequest", (message) => {
        if(message.body) {
          console.log("UBER CORRIDAS PARA ACEITAR: "+message.body);
         var temp = service.getStreamingCreatedEventRequest();
         console.log(temp);
        }
      });
    });
  }



}
