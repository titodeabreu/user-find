import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class DriverServiceService {
  private oboeService: any;
  private url;
  private eventSource :any;

  constructor(private http: HttpClient, private ngZone: NgZone, private eventSourceWhoKnows : EventSource) {
    this.url = '/api/driver/requests';
  }


  getStreamingCreatedEventRequest() {

    return Observable.create((observer) => {
      let eventSource = this.eventSource('/api/driver/requests');
      eventSource.onmessage = (event) => {
        console.log(event);
        console.log(JSON.parse(event.data));

        let data = JSON.parse(event.data);

        // $apply external (window.EventSource) event data
        this.ngZone.run(() => observer.next(data));
        
      }
      eventSource.onerror = (error) => observer.error(error);
    }).subscribe();
    // return this.http
    // .get("/api/driver/requests").subscribe(data =>  {
    //    return data
    //   });

    // return Observable.create((observer) => {
    //   let url = this.url;
    //   let eventSource = new EventSource(url);
    //   eventSource.onmessage = (event) => {
    //     console.log('Received event: ', event);
    //     let json = JSON.parse(event.data);
    //     console.log('json: ', json);
    //     observer.next(json);
    //   };
    //   eventSource.onerror = (error) => {
    //     // readyState === 0 (closed) means the remote source closed the connection,
    //     // so we can safely treat it as a normal situation. Another way of detecting the end of the stream
    //     // is to insert a special element in the stream of events, which the client can identify as the last one.
    //     if (eventSource.readyState === 0) {
    //       console.log('The stream has been closed by the server.');
    //       eventSource.close();
    //       let json = JSON.parse(error.data);
    //       console.log('json: ', json);
    //       observer.complete();
    //     } else {
    //       observer.error('EventSource error: ' + error);
    //     }
    //   }
    // });

    // // Observable.create
    // let value =
   
    // //.forEach(event => () => JSON.stringify(event));
    // // subscribe( value => 
    // // // onNext() function
    // //   console.log('value is ' + value),
    // // // onError() function
    // // error => console.log('error is ' + JSON.stringify(error)),
    // // // onComplete() function
    // // () => console.log('completed'));

    // console.log("registering message broker")
    // var config = {
    //   'url': '/api/driver/requests',
    //   'method': "GET",
    //   'body': '',
    //   'cached': false,
    //   'withCredentials': false
    // }
    // this.oboeService = oboe(config);
    // /* oboe(config)
    //   .done(function (things) {
    //     console.log("got");
    //     console.log(things);
    //     // we got it
    //   })
    //   .fail(function () {

    //     // we don't got it
    //   }); */

    // this.oboeService.node('!', function (thing) {
    //   console.log("new broker message", thing);
    // });
  }
}
